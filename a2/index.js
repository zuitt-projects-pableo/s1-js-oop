let students = ["John", "Joe", "Jane", "Jessie"]

// 1
 const addToEnd = (array, str) => {
 	if(typeof str === "string"){
 		array.push(str);
 		return array
 	} else {
 		return "error - can only add strings to an array"
 	}
 	
 }

// 2
const addToStart = (array, str) => {
 	if(typeof str === "string"){
 		array.unshift(str);
 		return array
 	} else {
 		return "error - can only add strings to an array"
 	}
 	
 }

 // 3
 const elementChecker = (array, str) => {
 	if(array.length !== 0){
 		return array.some(name => name === str)
 	} else {
 		return "error - passed in array is empty"
 	}
 	
 }

 // 4
 const checkAllStringsEnding = (array, end) => {
 	if(array.length === 0 ){
 		return "error - array must NOT be empty"
 	} else if(array.some(element => typeof element !== "string")){
 		return "error = all array elements must be strings"
 	} else if(typeof end !== "string"){
 		return "error - 2nd argument must be of data type string"
 	} else if(end.length > 1){
 		return "error - 2nd argument must be a single character"
 	} else {
 		return array.every(name => name[name.length - 1] === end)
 	}
 }

 // 5 
const stringLengthSorter = (array) => {
	if(array.some(element => typeof element !== "string")){
 		return "error = all array elements must be strings"
 	} else {
 		array.sort((a, b) => {
 			return a.length - b.length
 		})
 		console.log(array)
 	}
}

// 6
const startsWithCounter = (array, char) => {
	if(array.length === 0 ){
 		return "error - array must NOT be empty"
 	} else if(array.some(element => typeof element !== "string")){
 		return "error = all array elements must be strings"
 	} else if(typeof char !== "string"){
 		return "error - 2nd argument must be of data type string"
 	} else if(char.length > 1){
 		return "error - 2nd argument must be a single character"
 	} else {
 		let result = 0;

 		array.forEach(name => {
 			if(name[0].toLowerCase() === char.toLowerCase()){
 				result++
 			}
 		})
 		console.log(result)
 	}
}

// 7 
const likeFinder = (array, char) => {
	if(array.length === 0 ){
 		return "error - array must NOT be empty"
 	} else if(array.some(element => typeof element !== "string")){
 		return "error = all array elements must be strings"
 	} else if(typeof char !== "string"){
 		return "error - 2nd argument must be of data type string"
 	} else {
 		let result = [];

 		array.forEach(name => {
 			if(name.toLowerCase().includes(char.toLowerCase())){
 				result.push(name)
 			}
 		})
 		console.log(result)
 	}
}

// 8
const randomPicker = (array) => {
	console.log(array[Math.floor(Math.random() * array.length)])
}